import feedparser
import urllib2
import time
import pytz
from datetime import datetime
import os
import string
from django.conf import settings
from dateutil import parser

from .models import Channel


class Monster(object):
    def __init__(self, db_feed):

        self.db_feed = db_feed
        self.valid_chars = "-_.() %s%s" % (string.ascii_letters, string.digits)

        if self.db_feed.modified and len(self.db_feed.modified) > 0:
            self.web_feed = feedparser.parse(db_feed.url, modified=db_feed.modified)
        elif self.db_feed.etag and len(self.db_feed.etag) > 0:
            self.web_feed = feedparser.parse(db_feed.url, etag=db_feed.etag)
        else:
            self.web_feed = feedparser.parse(db_feed.url)

    def read_feed(self):
        """
            Download & update feed content.
        """
        try:
            self.logger.info("Reading feed: %d" % self.db_feed.pk)

            self.db_feed.last_checked = datetime.now(pytz.utc)

            # Feed is not well-formed
            if self.web_feed.bozo:
                return

            if hasattr(self.web_feed, 'status'):
                # feed marked as "gone"
                if self.web_feed.status == 410:
                    self.db_feed.is_active = False
                    self.db_feed.save()
                    return

            if not self.has_changes:
                return

            if hasattr(self.web_feed, 'etag'):
                self.db_feed.etag = self.web_feed.etag

            if hasattr(self.web_feed, 'modified'):
                self.db_feed.modified = self.web_feed.modified

            if not hasattr(self.db_feed, 'channel'):
                self.db_feed.channel = Channel.objects.create(feed=self.db_feed)
                self.db_feed.channel.save()

            self.fill_channel()
            self.create_entries()

            self.db_feed.save()

            self.logger.info("Finished eading feed: %d" % self.db_feed.pk)
        except Exception as e:
            msg = "Failed reading feed %d" % self.db_feed.pk
            if e.args:
                msg = "%s (%s)" % (msg, str(e.args))

            self.logger.error(msg)

    def fill_channel(self):
        """
            Fill RSS channel tag fields.
        """

        db_channel = self.db_feed.channel
        web_channel = self.web_feed.feed

        if hasattr(web_channel, 'language'):
            db_channel.language = web_channel.language

        if hasattr(web_channel, 'copyright'):
            db_channel.copyright = web_channel.copyright

        self.fill_common_fields(db_channel, web_channel)

        db_channel.save()

    def create_entries(self):
        """
            Fill RSS item (article) tag fields.
        """
        for entry in self.web_feed.entries:
            try:
                item = self.db_feed.channel.item_set.create(item_id=entry.id)

                if hasattr(entry, 'summary'):
                    item.description = entry.summary

                self.download_content(entry)
                self.fill_common_fields(item, entry)

                item.save()
            except Exception as e:
                msg = "Failed reading entry '%s' for feed %d\n" % (entry.id, self.db_feed.pk)
                if e.args:
                    msg = "%s (%s)" % (msg, str(e.args))

                self.logger.error(msg)

    def fill_common_fields(self, db_obj, web_obj):
        """
            Fill fields that are common to the channel and item tags.
        """

        if hasattr(web_obj, 'category'):
            db_obj.category = web_obj.category

        if hasattr(web_obj, 'link'):
            db_obj.link = web_obj.link

        if hasattr(web_obj, 'title'):
            db_obj.title = web_obj.title

        if hasattr(web_obj, 'author'):
            db_obj.author = web_obj.author

        if hasattr(web_obj, 'description'):
            db_obj.description = web_obj.description

        if hasattr(web_obj, 'updated'):
            db_obj.updated = parser.parse(web_obj.updated)

        if hasattr(web_obj, 'published'):
            db_obj.published = parser.parse(web_obj.published)

    def download_content(self, item):
        """
            Write the article to a file.
        """

        response = urllib2.urlopen(item.link)
        html = response.read()

        if not os.path.exists(settings.PATH_TO_ARTICLES):
            os.makedirs(settings.PATH_TO_ARTICLES)

        path = '{0}{1}.{2}.html'.format(settings.PATH_TO_ARTICLES,
                                        ''.join(
                                            c for c in item.title[0:250] if c in self.valid_chars),
                                        str(time.time()))

        article_file = open(path, 'w+')
        article_file.write(html)
        article_file.close

    @property
    def has_changes(self):
        return True if self.web_feed.status in [200, 302] else False