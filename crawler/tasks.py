from celery.utils.log import get_task_logger

from monster import Monster
from rssCrawl import celery_app
from models import Feed


@celery_app.task
def crawl():
    """
        Process all known (db) feeds as individual tasks.
    """

    feeds = Feed.objects.filter(is_active=True)

    for feed in feeds:
        process_feed.delay(feed)


@celery_app.task
def process_feed(feed):
    """
        Process an individual feed.
    """

    monster = Monster(feed)

    if not monster.has_changes:
        return

    monster.logger = get_task_logger("MonsterLogger")
    monster.logger.debug("Reading feed")
    monster.read_feed()

